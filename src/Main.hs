module Main where

import           Control.Monad (when, unless)
import           Control.Monad.Catch (catchIOError)
import           Crypto.Hash (hash, Digest, SHA1)
import qualified Data.ByteString.Lazy.Char8 as BSL
import           Data.Foldable (for_)
import           Data.Function (on)
import           Data.List (delete, groupBy, intercalate, nub, sort, sortOn)
import           Data.Maybe (mapMaybe)
import           Data.Text (Text)
import qualified Data.Text as Text
import qualified Data.Text.IO as Text
import qualified Data.Text.Encoding as Text
import           Data.Traversable (for)
import qualified Distribution.PackageDescription.Parse as Cabal
import           Distribution.PackageDescription.Configuration (flattenPackageDescription)
import           Distribution.Types.Dependency (depPkgName)
import           Distribution.Types.PackageDescription (PackageDescription(..))
import           Distribution.Types.PackageName (unPackageName)
import           Distribution.Types.PackageId (PackageIdentifier(..))
import           Distribution.Version (versionNumbers)
import           Network.HTTP.Simple (httpLBS, getResponseBody, parseRequest)
import           Text.Feed.Import (parseFeedSource)
import qualified Text.Feed.Query as Feed
import           Text.Feed.Types (Item)
import           System.Directory (XdgDirectory(..), createDirectoryIfMissing, getXdgDirectory, )
import           System.FilePath ((</>))

main :: IO ()
main = do
  cacheDir <- getXdgDirectory XdgCache "hackage-recent"
  createDirectoryIfMissing True cacheDir
  dataDir <- getXdgDirectory XdgData "hackage-recent"
  createDirectoryIfMissing True dataDir
  -- print (cacheDir, dataDir)

  recent <- fetchRecent
  packages <- mapM (processFeedItem cacheDir) recent

  mapM_ (updateStored dataDir) packages

  triggers <- checkTriggers dataDir packages

  bundles <- fmap (groupWith fst snd . mconcat) $ mapM (getSubscribers dataDir) triggers
  mapM_ (runNotify dataDir) bundles

  -- TODO: remove sent notifications

processFeedItem :: FilePath -> HackageItem -> IO PackageDescription
processFeedItem cacheDir item = do
  -- print (cabalUrl, cabalCached)

  cabal <- catchIOError (readFile cabalCached) $ \_ -> do
    -- cache miss
    req <- parseRequest (Text.unpack cabalUrl)
    res <- fmap getResponseBody (httpLBS req)
    BSL.writeFile cabalCached res
    pure (BSL.unpack res)

  case Cabal.parseGenericPackageDescription cabal of
    Cabal.ParseFailed err ->
      fail $ "cabal parsing failed: " ++ show err
    Cabal.ParseOk _ gpd ->
      pure $ flattenPackageDescription gpd
  where
    cabalUrl = hackageCabalUrl item
    cabalUrlDigest = show @(Digest SHA1) . hash $ Text.encodeUtf8 cabalUrl
    cabalCached = cacheDir </> cabalUrlDigest

updateStored :: FilePath -> PackageDescription -> IO ()
updateStored dataDir pkg = do
  let dependsOnPath = dataDir </> "depends-on"
  let usedByPath = dataDir </> "used-by"
  createDirectoryIfMissing True dependsOnPath
  createDirectoryIfMissing True usedByPath

  -- print (name, version, deps)

  writeFile (usedByPath </> name) (unlines deps)

  for_ deps $ \depName -> do
    let depPath = dependsOnPath </> depName
    usedByText <- catchIOError (Text.readFile depPath) $ \_ ->
      pure mempty
    let usedBy = Text.lines usedByText

    when (Text.pack name `notElem` usedBy) $
      Text.writeFile depPath . Text.unlines . nub . sort $
        Text.pack name : usedBy

  where
    (name, _) = simplePackage pkg
    deps = packageDepends pkg

simplePackage :: PackageDescription -> (String, String)
simplePackage (package -> pkg) =
  ( unPackageName $ pkgName pkg
  , intercalate "." . map show . versionNumbers $ pkgVersion pkg
  )

type PackageName = String
type Version = String
type Trigger = (PackageName, [Version])
type Triggered = (PackageName, [Trigger])

-- | Get a package to notify and list of what's changed
checkTriggers :: FilePath -> [PackageDescription] -> IO [Triggered]
checkTriggers dataDir packages = do
  triggers <- mapM scan packages

  let recentPkgs = sort $ map simplePackage packages
  pure . collect [] $ sort
    [ (pkg, by, [ pv | (pn, pv) <- recentPkgs, pn == by ])
    | (pkgs, by) <- nub $ filter (not . null . fst) triggers
    , pkg <- pkgs
    ]

  where
    scan (simplePackage -> (name, _)) = do
      let dependsOnMePath = dataDir </> "depends-on" </> name
      dependsOn' <- catchIOError (Text.readFile dependsOnMePath) $ \_ ->
        pure mempty
      pure
        ( delete name . map Text.unpack $ Text.lines dependsOn'
        , name
        )

    collect acc [] = sort [ (pkg, sort [ (dep, sort ver) | (dep, ver) <- deps ] ) | (pkg, deps) <- acc ]
    collect [] ((pkg, by, ver) : next) = collect [(pkg, [(by, ver)])] next
    collect (prev@(prevPkg, prevBy) : acc) ((pkg, by, ver) : next)
      | prevPkg == pkg = collect ((pkg, (by, ver) : prevBy) : acc) next
      | otherwise      = collect ((pkg, [(by, ver)]) : prev : acc) next

type SubscriberId = Text
type Channel = Text

getSubscribers :: FilePath -> Triggered -> IO [(Channel, (SubscriberId, Triggered))]
getSubscribers dataDir triggered = do
  subIds <- catchIOError loadSubs (const $ pure ["563df26e-a152-49dc-99f5-6b2b7553ac51"])
  fmap unwrap (mapM bundle subIds)
  where
    loadSubs = fmap Text.lines $ Text.readFile pkgSubsPath
    pkgSubsPath = dataDir </> "subscribed" </> fst triggered
    subDataPath subId = dataDir </> "subscribers" </> Text.unpack subId

    bundle subId = do
      -- print (pkg, subId, deps)

      let loadChans = fmap Text.lines $ Text.readFile (subDataPath subId)
      chans <- catchIOError loadChans (const $ pure ["dump"])

      pure (subId, (chans, triggered))

    unwrap items =
      [ (chan, (subId, tgd))
      | (subId, (chans, tgd)) <- items
      , chan <- chans
      ]

type Seen = (SubscriberId, [(PackageName, Version)])

runNotify :: FilePath -> (Channel, [(SubscriberId, Triggered)]) -> IO () --[(Channel, Seen)]
runNotify dataDir (chan, bundle) =
  case chan of
    "dump" -> notifyDump dataDir bundle
    _      -> fail $ "Unknown notification channel: " ++ show chan

notifyDump :: FilePath -> [(SubscriberId, Triggered)] -> IO ()
notifyDump dataDir bundle = do
  putStrLn "Dumping bundle..."
  for_ (groupWith fst snd bundle) $ \(subId, triggered) -> do
    putStrLn $ "  Notifications for " ++ show subId
    for_ triggered $ \(pkg, triggers) ->
      withUnseen dataDir "dump" subId triggers $ \newTriggers -> do
        putStrLn $ "    " ++ pkg
        for_ newTriggers $ \(trigName, versions) ->
          putStrLn $ "      " ++ trigName ++ ": " ++ intercalate ", " versions

withUnseen
  :: FilePath
  -> Channel
  -> SubscriberId
  -> [Trigger]
  -> ([Trigger] -> IO ())
  -> IO ()
withUnseen dataDir chan subId triggers proc = do
  unseen <- for triggers $ \(trigName, versions) -> do
    checked <- for versions $ \ver -> do
      let check = fmap (const True) (Text.readFile $ markPath trigName ver)
      catchIOError check $
        -- Check mark not found
        const (pure False)
    pure
      [ (trigName, ver)
      | (ver, seen) <- zip versions checked
      , not seen
      ]
  let newTriggers = groupWith fst snd $ concat unseen
  unless (null newTriggers) $ do
    proc newTriggers
    createDirectoryIfMissing True chanSubPath
    for_ newTriggers $ \(trigName, versions) ->
      for_ versions $ \ver ->
        Text.writeFile (markPath trigName ver) mempty
  where
    chanSubPath = dataDir </> "seen" </> Text.unpack chan </> Text.unpack subId
    markPath pkg ver = chanSubPath </> pkg ++ "-" ++ ver

packageDepends :: PackageDescription -> [String]
packageDepends = nub . sort . map (unPackageName . depPkgName) . buildDepends

fetchRecent :: IO [HackageItem]
fetchRecent = do
  res <- httpLBS "https://hackage.haskell.org/recent.rss"
  case parseFeedSource (getResponseBody res) of
    Just feed ->
      pure . reverse $ mapMaybe hackageItem (Feed.getFeedItems feed)
    Nothing ->
      fail "Failed to get the feed."

data HackageItem = HackageItem
  { title       :: Text
  , link        :: Text
  , summary     :: Text
  , publishDate :: Text
  } deriving (Show)

hackageItem :: Item -> Maybe HackageItem
hackageItem item = HackageItem
  <$> Feed.getItemTitle item
  <*> Feed.getItemLink item
  <*> Feed.getItemSummary item
  <*> Feed.getItemPublishDateString item

hackageCabalUrl :: HackageItem -> Text
hackageCabalUrl item =
  case Text.words (title item) of
    [pkgName, _pkgVer] ->
      mconcat
        [ link item
        , "/"
        , pkgName
        , ".cabal"
        ]
    items ->
      error $ "assert: invalid title format: " ++ show items

groupWith :: Ord key => (a -> key) -> (a -> value) -> [a] -> [(key, [value])]
groupWith look pick = collect . groupBy by . sortOn look
  where
    by = (==) `on` look
    collect groups =
      [ (look first, map pick group)
      | group@( first : _) <- groups
      ]
